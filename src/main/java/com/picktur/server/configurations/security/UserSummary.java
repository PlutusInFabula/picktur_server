package com.picktur.server.configurations.security;

import lombok.Data;

@Data
public class UserSummary {
    private String id;
    private String username;
    private String name;

    public UserSummary(String id, String username, String name) {
        this.id = id;
        this.username = username;
        this.name = name;
    }
}
