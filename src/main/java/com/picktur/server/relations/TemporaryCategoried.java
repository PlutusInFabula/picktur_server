package com.picktur.server.relations;

import com.arangodb.springframework.annotation.Edge;
import com.arangodb.springframework.annotation.From;
import com.arangodb.springframework.annotation.To;
import com.picktur.server.entities.Category;
import com.picktur.server.entities.photo_upload.TemporaryPhoto;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Edge
@Data
public class TemporaryCategoried {
    @Id
    private String id;
    @From
    private Category category;
    @To
    private TemporaryPhoto categoriedPhoto;

}
