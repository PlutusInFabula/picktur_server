package com.picktur.server.relations;

import com.arangodb.springframework.annotation.Edge;
import com.arangodb.springframework.annotation.From;
import com.arangodb.springframework.annotation.To;
import com.picktur.server.entities.photo_upload.TemporaryPhoto;
import com.picktur.server.entities.photo_upload.TemporaryPhotoCollection;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Edge
@Data
public class TemporaryCollectioned {
    @Id
    private String id;
    @From
    private TemporaryPhotoCollection collection;
    @To
    private TemporaryPhoto collectionedPhoto;

}
