package com.picktur.server.relations;

import com.arangodb.springframework.annotation.Edge;
import com.arangodb.springframework.annotation.From;
import com.arangodb.springframework.annotation.To;
import com.picktur.server.entities.Authorization;
import com.picktur.server.entities.Photo;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Edge
@Data
public class Authorized {

    @Id
    private String id;

    @From
    private Authorization authorization;

    @To
    private Photo authorizedPhoto;

}
