package com.picktur.server.relations;

import com.arangodb.springframework.annotation.Edge;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Edge
@Data
public class View {

    @Id
    private String id;


}
