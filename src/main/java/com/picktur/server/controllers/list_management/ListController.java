package com.picktur.server.controllers.list_management;

import com.picktur.server.entities.Tag;
import com.picktur.server.entities.dto.PhotoDto;
import com.picktur.server.entities.dto.TagDto;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Random;

@Data
@RestController
@RequestMapping("/api/public/lists")
public class ListController {

    @GetMapping("/homepage")
    public Page<PhotoDto> getPhotoForHomePage() {
        ArrayList<PhotoDto> resultList = new ArrayList<>();

        PhotoDto p1 = new PhotoDto();
        p1.setId("1:01");
        p1.setOwnerId("1:02");
        p1.setOwner("Andrea Taini");
        p1.setTitle("Great picture");
        p1.setDesctiption("Some description");
        p1.setWeight(new Random().nextDouble()*5);
        p1.setUrl_fr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/A_FR.jpg");
        p1.setUrl_hr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/a_HR.jpg");
        p1.setUrl_mr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/a_MR.jpg");
        p1.setUrl_lr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/a_LR.jpg");
        p1.setRating(new Random().nextDouble()*5);

        PhotoDto p2 = new PhotoDto();
        p2.setId("2:01");
        p2.setOwnerId("2:02");
        p2.setOwner("Andrea Piacquadio");
        p2.setTitle("Nice picture");
        p2.setDesctiption("Can go");
        p2.setWeight(new Random().nextDouble()*5);
        p2.setUrl_fr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/B_FR.jpg");
        p2.setUrl_hr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/B_HR.jpg");
        p2.setUrl_mr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/B_MR.jpg");
        p2.setUrl_lr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/B_LR.jpg");
        p2.setRating(new Random().nextDouble()*5);

        PhotoDto p3 = new PhotoDto();
        p3.setId("3:01");
        p3.setOwnerId("3:02");
        p3.setOwner("Jessica Ticozzelli");
        p3.setTitle("Super picture");
        p3.setDesctiption("It is a super good picture");
        p3.setWeight(new Random().nextDouble()*5);
        p3.setUrl_fr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/C_FR.jpg");
        p3.setUrl_hr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/C_HR.jpg");
        p3.setUrl_mr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/C_MR.jpg");
        p3.setUrl_lr("https://picktur.s3.eu-central-1.amazonaws.com/pictures/C_LR.jpg");
        p3.setRating(new Random().nextDouble()*5);

        resultList.add(p1);
        resultList.add(p2);
        resultList.add(p3);
        resultList.addAll(resultList);
        resultList.addAll(resultList);
        resultList.addAll(resultList);
        resultList.addAll(resultList);
        resultList.addAll(resultList);
        resultList.addAll(resultList);
        resultList.addAll(resultList);
        resultList.addAll(resultList);

        Page result = new PageImpl(resultList);
        return result;
    }



    private ArrayList<TagDto> populateResulList(ArrayList<Tag> tagList) {
        ArrayList<TagDto> resultList = new ArrayList();

        for (Tag t: tagList) {
            TagDto actualTagDto = new TagDto(t.getId(), t.getValue(), new Random().nextDouble());
            resultList.add(actualTagDto);
        }
        return resultList;
    }
}
