package com.picktur.server.controllers.photo_upload;

import com.picktur.server.entities.dto.photo_upload.TemporaryPhotoCollectionDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MultiplePhotoUploadResponse {

    private TemporaryPhotoCollectionDto collection;

}
