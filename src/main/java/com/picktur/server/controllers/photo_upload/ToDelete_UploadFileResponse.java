package com.picktur.server.controllers.photo_upload;

import lombok.Data;

@Data
public class ToDelete_UploadFileResponse {
    private String fileName;
    private String fileType;
    private long size;

    public ToDelete_UploadFileResponse(String fileName, String fileType, long size) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.size = size;
    }

}