package com.picktur.server.controllers.photo_upload;

import com.picktur.server.entities.Category;
import com.picktur.server.entities.Tag;
import com.picktur.server.entities.dto.CategoryDto;
import com.picktur.server.entities.dto.TagDto;
import com.picktur.server.entities.dto.photo_upload.TemporaryAuthorizationDto;
import com.picktur.server.entities.dto.photo_upload.TemporaryPhotoCollectionDto;
import com.picktur.server.entities.dto.photo_upload.TemporaryPhotoDto;
import com.picktur.server.entities.photo_upload.TemporaryAuthorization;
import com.picktur.server.entities.photo_upload.TemporaryPhoto;
import com.picktur.server.entities.photo_upload.TemporaryPhotoCollection;
import com.picktur.server.services.FileStorageService;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

@Data
@RestController
@RequestMapping("/api/photos")
public class PhotoUploaderController {

    private static final Logger logger = LoggerFactory.getLogger(PhotoUploaderController.class);

    @Autowired
    private FileStorageService fileStorageService;


    public ToDelete_UploadFileResponse uploadFile(MultipartFile file) {
        String fileName = fileStorageService.uploadFile(file); //storeFile(file);
        String serverAddress = "https://picktur.s3.eu-central-1.amazonaws.com/";
        String pattern = "(.*?)amazonaws.com\\/picktur\\/";

        fileName = fileName.replaceAll(pattern, serverAddress);

        System.out.println("Filename: " + fileName);

        return new ToDelete_UploadFileResponse(fileName, file.getContentType(), file.getSize());
    }

    @PostMapping("/uploadMultipleFiles")
    public MultiplePhotoUploadResponse uploadMultipleFiles(@RequestBody MultiplePhotoUploadRequest request) {

        TemporaryPhotoCollection collection = new TemporaryPhotoCollection();
        collection.setName(request.getCollection());
        collection.setCreationInstant(Instant.now());

        ArrayList<TemporaryPhoto> collectionedPhotos = new ArrayList<>();

        // Batching files to upload onto server
        /*Arrays.asList(request.getFiles())
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
         */

        collection.setPhotos(collectionedPhotos);
        //Persist collection
        //Persist photos

        //Populate collectionDto for Response
        TemporaryPhotoCollectionDto collectionDto = populateCollectionDto(collection);
        MultiplePhotoUploadResponse response = new MultiplePhotoUploadResponse(collectionDto);

        return response;
    }

    private TemporaryPhotoCollectionDto populateCollectionDto(TemporaryPhotoCollection collection) {
        TemporaryPhotoCollectionDto collectionDto = new TemporaryPhotoCollectionDto();
        collectionDto.setId(collection.getId());
        collectionDto.setCreationInstant(collection.getCreationInstant());
        collectionDto.setName(collection.getName());

        // Populate PhotoDto List
        ArrayList<TemporaryPhotoDto> collectedPhotosDto = populatePhotoDtoList(collection);

        collectionDto.setPhotos(collectedPhotosDto);
        return collectionDto;
    }

    private ArrayList<TemporaryPhotoDto> populatePhotoDtoList(TemporaryPhotoCollection collection) {
        ArrayList<TemporaryPhotoDto> collectedPhotosDto = new ArrayList<>();

        collection.getPhotos().forEach(p -> {
            TemporaryPhotoDto photoDto = new TemporaryPhotoDto();
            photoDto.setId(p.getId());
            photoDto.setUploadInstant(p.getUploadInstant());
            photoDto.setUrl_lr(p.getUrl_lr());
            photoDto.setUrl_hr(p.getUrl_hr());
            photoDto.setUrl_mr(p.getUrl_mr());
            photoDto.setUrl_hr(p.getUrl_hr());
            photoDto.setTitle(p.getTitle());
            photoDto.setDesctiption(p.getDesctiption());
            // Populate ContainedDTO
            photoDto.setContainedTags(populateTagDtoList(p.getContainedTags()));
            // Populate AssignedTagsDTO
            photoDto.setTags(populateTagDtoList(p.getTags()));
            // Populate CategoryDTO
            photoDto.setCategories(populateCategoryDtoList(p.getCategories()));
            // Populate AuthorizationDTO
            photoDto.setAuthorizations(populateAuthorizationDtoList(p.getAuthorizations()));
            photoDto.setRating(p.getRating());
            photoDto.setWeight(p.getWeight());
            photoDto.setOwner(p.getPhotoOwner().getUsername());
            photoDto.setOwnerId(p.getPhotoOwner().getId());
            collectedPhotosDto.add(photoDto);
        });
        return collectedPhotosDto;
    }

    private ArrayList<TagDto> populateTagDtoList(Collection<Tag> p) {
        ArrayList<TagDto> tagsToConvert = new ArrayList<>();
        p.forEach(tag -> {
            TagDto tagDto = new TagDto();
            tagDto.setValue(tag.getValue());
            tagsToConvert.add(tagDto);
        });
        return tagsToConvert;
    }

    private ArrayList<CategoryDto> populateCategoryDtoList(Collection<Category> p) {
        ArrayList<CategoryDto> categoriesToConvert = new ArrayList<>();
        p.forEach(category -> {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.setValue(category.getValue());
            categoriesToConvert.add(categoryDto);
        });
        return categoriesToConvert;
    }

    private ArrayList<TemporaryAuthorizationDto> populateAuthorizationDtoList(Collection<TemporaryAuthorization> p) {
        ArrayList<TemporaryAuthorizationDto> authorizationsToConvert = new ArrayList<>();
        p.forEach(temporaryAuthorization -> {
            TemporaryAuthorizationDto temporaryAuthorizationDto = new TemporaryAuthorizationDto();
            temporaryAuthorizationDto.setDocumentUrl(temporaryAuthorization.getDocumentUrl());
            temporaryAuthorizationDto.setId(temporaryAuthorization.getId());
            authorizationsToConvert.add(temporaryAuthorizationDto);
        });
        return authorizationsToConvert;
    }

}
