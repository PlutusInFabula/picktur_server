package com.picktur.server.controllers.photo_upload;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class MultiplePhotoUploadRequest {

    private String collection;
    private MultipartFile[] files;

}
