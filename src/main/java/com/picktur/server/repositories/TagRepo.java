package com.picktur.server.repositories;

import com.arangodb.springframework.repository.ArangoRepository;
import com.picktur.server.entities.Tag;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface TagRepo extends ArangoRepository<Tag, String> {
    Optional<Tag> findByValue(String name);

//@Query("FOR t IN tags FILTER t.value like '@word'")
    List<Tag> readAllByValueIgnoreCaseContaining(String word);

    ArrayList<Tag> findByValueContainsAllIgnoreCase(String word);

}
