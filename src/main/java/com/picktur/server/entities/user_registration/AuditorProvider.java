package com.picktur.server.entities.user_registration;

import com.picktur.server.entities.User;
import lombok.Data;
import org.springframework.data.domain.AuditorAware;
import java.util.Optional;


@Data
public class AuditorProvider /*implements AuditorAware<User>*/ {

    private User user;

    //@Override
    //public Optional<User> getCurrentAuditor() {
    //    return null; // Optional.ofNullable(user);
    //}
}