package com.picktur.server.entities;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Relations;
import com.picktur.server.relations.*;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Collection;

//@HashIndex(fields = { "id" }, unique = true)
@Document("photos")
@Data
public class Photo {

    @Id
    private String id;

    private String url_lr;
    private String url_mr;
    private String url_hr;
    private String url_fr;

    private String title;
    private String desctiption;

    private boolean published;

    @Relations(edges = Tagged.class, lazy = true)
    private Collection<Tag> tags;

    @Relations(edges = Categoried.class, lazy = true)
    private Collection<Category> categories;

    @Relations(edges = Authorized.class, lazy = true)
    private Collection<Authorization> Authorized;

    @Relations(edges = Upload.class, lazy = true)
    private User photoOwner;

    @Relations(edges = Download.class, lazy = true)
    private Collection<User> downloaderUsers;

    @Relations(edges = Collectioned.class, lazy = true)
    private PhotoCollection photoCollection;

}
