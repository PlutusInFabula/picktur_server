package com.picktur.server.entities;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Relations;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Collection;

@Document("authorizations")
@Data
public class Authorization {

    @Id
    private String id;

    @Relations(edges = com.picktur.server.relations.Authorized.class, lazy = true)
    private Collection<Photo> Authorized;

    private String documentUrl;
}
