package com.picktur.server.entities.dto.photo_upload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.time.Instant;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TemporaryPhotoCollectionDto {

    @Id
    private String id;

    private String name;
    private Instant creationInstant;

    private Collection<TemporaryPhotoDto> photos;

}
