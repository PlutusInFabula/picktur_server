package com.picktur.server.entities.dto.photo_upload;

import com.picktur.server.entities.dto.CategoryDto;
import com.picktur.server.entities.dto.TagDto;
import lombok.Data;

import java.time.Instant;
import java.util.Collection;

@Data
public class TemporaryPhotoDto {

    private String id;

    private Instant uploadInstant;

    private String url_lr;
    private String url_mr;
    private String url_hr;
    private String url_fr;

    private String title;
    private String desctiption;

    private Collection<TagDto> containedTags;
    private Collection<TagDto> tags;
    private Collection<CategoryDto> categories;
    private Collection<TemporaryAuthorizationDto> Authorizations;

    private double rating;
    private double weight;

    private String ownerId;
    private String owner;

}
