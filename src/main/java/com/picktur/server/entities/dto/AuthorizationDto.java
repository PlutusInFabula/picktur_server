package com.picktur.server.entities.dto;

import com.picktur.server.entities.Photo;
import lombok.Data;

import java.util.Collection;

@Data
public class AuthorizationDto {

    private String id;

    private Collection<Photo> Authorized;

    private String documentUrl;
}
