package com.picktur.server.entities.dto.photo_upload;

import lombok.Data;

import java.util.Collection;

@Data
public class TemporaryAuthorizationDto {

    private String id;

    private Collection<TemporaryPhotoDto> authorizedPhotos;

    private String documentUrl;
}
