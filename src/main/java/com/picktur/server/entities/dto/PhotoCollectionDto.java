package com.picktur.server.entities.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.Instant;
import java.util.Collection;

@Data
public class PhotoCollectionDto {

    @Id
    private String id;

    private String name;
    private Instant creationInstant;
    private boolean published;

    private Collection<PhotoDto> photos;

}
