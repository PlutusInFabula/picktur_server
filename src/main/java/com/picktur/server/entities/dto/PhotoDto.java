package com.picktur.server.entities.dto;

import lombok.Data;

import java.time.Instant;
import java.util.Collection;

@Data
public class PhotoDto {

    private String id;

    private Instant uploadInstant;

    private boolean published;

    private String url_lr;
    private String url_mr;
    private String url_hr;
    private String url_fr;

    private String title;
    private String desctiption;

    private Collection<TagDto> tags;
    private Collection<CategoryDto> categories;

    private double rating;
    private double weight;

    private String ownerId;
    private String owner;

}
