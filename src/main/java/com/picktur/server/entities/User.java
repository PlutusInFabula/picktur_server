package com.picktur.server.entities;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.HashIndex;
import com.arangodb.springframework.annotation.Ref;
import com.arangodb.springframework.annotation.Relations;
import com.picktur.server.entities.user_registration.Role;
import com.picktur.server.relations.Download;
import com.picktur.server.relations.Following;
import com.picktur.server.relations.Upload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.*;

import java.time.Instant;
import java.util.*;

@HashIndex(fields = { "name", "surname" }, unique = true)
@Document("users")
@Data @NoArgsConstructor
public class User {

    @Id
    private String id;

    private String username;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String avatar;
    private String description;
    private String location;
    private String instagram;
    private String facebook;
    @Relations(edges = Following.class, lazy = true)
    private Collection<User> following;
    private Collection<PhotoCollection> collections;

    @Relations(edges = Upload.class, lazy = true)
    private Collection<Photo> upLoadedPhotos;
    @Relations(edges = Download.class, lazy = true)
    private Collection<Photo> downLoadedPhotos;

    @Ref
    private List<Role> roles = new ArrayList();
    @CreatedDate
    private Instant created;
   // @CreatedBy
   // private User createdBy;
    @LastModifiedDate
    private Instant modified;

    public User(String name, String username, String email, String password) {
        this.name=name;
        this.username=username;
        this.email=email;
        this.password=password;
    }
    // @LastModifiedBy
   // private User modifiedBy;
}
