package com.picktur.server.entities.photo_upload;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Relations;
import com.picktur.server.entities.Category;
import com.picktur.server.entities.Tag;
import com.picktur.server.entities.User;
import com.picktur.server.relations.*;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.Instant;
import java.util.Collection;

//@HashIndex(fields = { "id" }, unique = true)
@Document("photos")
@Data
public class TemporaryPhoto {

    @Id
    private String id;

    private Instant uploadInstant;

    private String url_lr;
    private String url_mr;
    private String url_hr;
    private String url_fr;

    private String title;
    private String desctiption;

    @Relations(edges = TemporaryContainedTagged.class, lazy = true)
    private Collection<Tag> containedTags;

    @Relations(edges = TemporaryTagged.class, lazy = true)
    private Collection<Tag> tags;

    @Relations(edges = TemporaryCategoried.class, lazy = true)
    private Collection<Category> categories;

    @Relations(edges = TemporaryAuthorized.class, lazy = true)
    private Collection<TemporaryAuthorization> Authorizations;

    private double rating;
    private double weight;

    @Relations(edges = TemporaryUpload.class, lazy = true)
    private User photoOwner;

    @Relations(edges = TemporaryCollectioned.class, lazy = true)
    private TemporaryPhotoCollection photoCollection;

}
