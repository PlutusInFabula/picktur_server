package com.picktur.server.entities.photo_upload;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Relations;
import com.picktur.server.relations.TemporaryCollectioned;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.Instant;
import java.util.Collection;

@Document("photoCollections")
@Data
public class TemporaryPhotoCollection {

    @Id
    private String id;
    private String name;
    private Instant creationInstant;

    @Relations(edges = TemporaryCollectioned.class, lazy = true)
    private Collection<TemporaryPhoto> photos;

}
