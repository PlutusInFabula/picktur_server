package com.picktur.server.entities.photo_upload;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Relations;
import com.picktur.server.relations.TemporaryAuthorized;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Collection;

@Document("authorizations")
@Data
public class TemporaryAuthorization {

    @Id
    private String id;

    @Relations(edges = TemporaryAuthorized.class, lazy = true)
    private Collection<TemporaryPhoto> authorizedPhotos;

    private String documentUrl;
}
